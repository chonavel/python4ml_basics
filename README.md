# Python4ML_basics

Introduction to Python, numpy, Scipy and Matplotlib

## Python and notebooks installation via miniconda

In order to work with a comfortable Python environment, we suggest that you install miniconda on your machine and create a conda environment that you can use for the labs. Conda is a popular way to create python environments. Miniconda supplies a minimal conda environnement.

Install miniconda from https://docs.conda.io/en/latest/miniconda.html

In general, for Mac, Linux and Windows, installation will be done in home directory (single user install) or directory /opt.

Then, "conda" command can be launched from a terminal (if the path to the command is not recognized it could be found in directory miniconda3/bin/conda). For Windows os a Powershell is created that can be started from the command menu.

- creating an environnement and installation of libraries used in the lessons:

    $ conda create -n LAB_3.9  python=3.9 jupyter numpy scipy matplotlib sympy numba pandas openpyxl scikit-learn

- activation of the environnement:

    $ conda activate LAB_3.9

- launching notebooks:

    $ jupyter notebook

- More info at https://docs.conda.io/projects/conda/en/latest/user-guide/index.html


