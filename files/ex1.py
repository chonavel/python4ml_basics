#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
- height at time t of an object falling from h0 with initial speed v0 
- function arguments: h0,v0,t

"""
import sys
g  = 9.81
h0 = float(sys.argv[1])
v0 = float(sys.argv[2])
t  = float(sys.argv[3])
print('height after %g s : %2.2f m' % (t,-1/2*g*t**2+v0*t+h0) )
